Example `secrets/local.env.yaml` file:

```yaml
---
logger:
  level: info
gitlab:
  rnd:
    url: https://rnd-gitlab.mobica.com
    token: my-token
    user: mylg
```