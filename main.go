/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/cmd"

func main() {
	cmd.Execute()
}
