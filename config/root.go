package config

import (
	"errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"time"
)

type LoggerConfig struct {
	Level string `mapstructure:"level"`
}

type GitlabConfig struct {
	Url   string `mapstructure:"url"`
	Token string `mapstructure:"token"`
	User  string `mapstructure:"user"`
}

type Config struct {
	Environment string                  `mapstructure:"env"`
	Gitlab      map[string]GitlabConfig `mapstructure:"gitlab"`
	Logger      LoggerConfig            `mapstructure:"logger"`
}

func LoadConfig() (*Config, error) {
	var config *Config

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	if err := viper.Unmarshal(&config); err != nil {
		log.Err(err).
			Str("file", viper.ConfigFileUsed()).
			Msg("Broken config file")
		return config, err
	}
	if config.Logger.Level == "" {
		log.Error().Msg("Missing config file")
		time.Sleep(5 * time.Minute)
		return config, errors.New("missing config file")
	}
	return config, nil
}
