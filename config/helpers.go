package config

import (
	"github.com/spf13/viper"
	"os"
)

func getTestConfig() (*Config, error) {
	cfgFile := os.Getenv("CFG_FILE")
	viper.SetConfigFile(cfgFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return LoadConfig()
}
