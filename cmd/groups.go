package cmd

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/app"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/config"
	"os"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

var gitlabServer string
var gid int

// groupsCmd represents the groups command
var groupsCmd = &cobra.Command{
	Use:   "groups",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		config, err := config.LoadConfig()
		if err == nil {
			if val, ok := config.Gitlab[gitlabServer]; ok {
				git, err := gitlab.NewClient(val.Token, gitlab.WithBaseURL(fmt.Sprintf("%s/api/v4", val.Url)))
				if err != nil {
					log.Err(err).Str("server", val.Url).Msg("Error creating client")
					os.Exit(1)
				}
				if gid != 0 {
					_, err = app.GetGroupById(git, gid, config.Gitlab[gitlabServer])
					if err != nil {
						log.Err(err).Str("server", git.BaseURL().Host).Msg("Error getting group")
						os.Exit(1)
					}
				} else {
					err = app.GetGroups(git, config.Gitlab[gitlabServer])
					if err != nil {
						log.Err(err).Str("server", git.BaseURL().Host).Msg("Error getting groups")
						os.Exit(1)
					}
				}
			} else {
				log.Error().Msg(fmt.Sprintf("Key: %s do not exists", gitlabServer))
			}
		} else {
			log.Err(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(groupsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	groupsCmd.PersistentFlags().StringVarP(&gitlabServer, "gitlab-server", "g", "", "Gitlab server to use by key name")
	groupsCmd.PersistentFlags().IntVarP(&gid, "group-id", "i", 0, "Gitlab server to use by key name")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// groupsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
