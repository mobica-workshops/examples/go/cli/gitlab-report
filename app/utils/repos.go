package utils

import (
	"fmt"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"github.com/spf13/afero"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/config"
	"os"
	"os/exec"
	"strings"
)

func GetProjectsRepos(project *gitlab.Project, config config.GitlabConfig) (*object.Commit, error) {
	var commit *object.Commit
	err := cloneOrPullRepo(project, config)
	if err != nil {
		return commit, err
	}

	r, err := git.PlainOpen(fmt.Sprintf("./_data/%s", project.PathWithNamespace))
	if err != nil {
		return commit, err
	}

	cIter, err := r.Log(&git.LogOptions{
		From: plumbing.Hash{},
	})
	if err != nil {
		return commit, err
	}

	err = cIter.ForEach(func(c *object.Commit) error {
		commit = c
		return storer.ErrStop
	})

	return commit, nil
}

func cloneOrPullRepo(project *gitlab.Project, config config.GitlabConfig) error {
	repo := fmt.Sprintf("%s%s%s", project.HTTPURLToRepo[:8], fmt.Sprintf("%s:%s@", config.User, config.Token), project.HTTPURLToRepo[8:])
	targetPath := fmt.Sprintf("./_data/%s", project.Namespace.FullPath)
	projectPath := fmt.Sprintf("./_data/%s/%s", project.Namespace.FullPath, project.Path)
	cmdClone := fmt.Sprintf("git -C %s clone %s", targetPath, repo)
	cmdPull := fmt.Sprintf("git -C %s pull %s", projectPath, repo)

	//log.Info().
	//	Str("repo", repo).
	//	Str("clone", cmdClone).
	//	Str("pull", cmdPull).
	//	Str("fullPath", projectPath).
	//	Msg("TEST")

	appFs := afero.NewOsFs()
	err := appFs.MkdirAll(targetPath, 0755)
	if err != nil {
		return err
	}

	if stat, err := os.Stat(projectPath); err == nil && stat.IsDir() {
		subErr := runCommand(cmdPull)
		if subErr != nil {
			//return subErr
			return nil
		}
	} else {
		subErr := runCommand(cmdClone)
		if subErr != nil {
			return subErr
		}
	}

	return nil
}

func runCommand(cmdToRun string) error {
	cmdArgs := strings.Fields(cmdToRun)
	cmd := exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...)
	//cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
