package utils

import (
	"fmt"
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/app/types"
)

func PrintGroups(groups []types.GroupRowData) {
	t := table.NewWriter()
	header := table.Row{"Group ID", "Group Name", "Group Path"}
	t.AppendHeader(header)
	for _, group := range groups {
		t.AppendRow(table.Row{group.Group.ID, group.Group.Name, group.Group.FullPath})
	}
	fmt.Println(t.Render())
}

func PrintProjects(projects []types.ProjectRowData) {
	//var members []string
	t := table.NewWriter()
	header := table.Row{"Group Path", "Project ID", "Project name", "Last Commit", "Last Commit Date"}
	t.AppendHeader(header)
	for _, project := range projects {
		//for _, user := range project.Users {
		//	members = append(members, user.Username)
		//}
		// strings.Join(members, ", "),
		t.AppendRow(
			table.Row{
				project.Group.FullPath,
				project.Project.ID,
				project.Project.Name,
				project.Commit.Author.String(),
				project.Commit.Author.When.String(),
			})
	}
	fmt.Println(t.Render())
}
