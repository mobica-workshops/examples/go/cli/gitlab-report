package utils

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"github.com/xuri/excelize/v2"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/app/types"
)

func GenerateGroupsReport(groups []types.GroupRowData) error {
	f := excelize.NewFile()
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()
	sheetName := "Sheet1"
	err := f.SetCellValue(sheetName, "A1", "Group ID")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "B1", "Group Name")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "C1", "Last Commit Date")
	if err != nil {
		return err
	}
	for id, group := range groups {
		err = f.SetCellValue(sheetName, fmt.Sprintf("A%d", id+2), group.Group.ID)
		if err != nil {
			return err
		}
		err = f.SetCellValue(sheetName, fmt.Sprintf("B%d", id+2), group.Group.Name)
		if err != nil {
			return err
		}
		if group.Commit != nil {
			err = f.SetCellValue(sheetName, fmt.Sprintf("C%d", id+2), group.Commit.Author.When.String())
			if err != nil {
				return err
			}
		}
	}
	if err = f.SaveAs("_data/groups.xlsx"); err != nil {
		return err
	}
	return nil
}

func GenerateProjectsReport(group *gitlab.Group, projects []types.ProjectRowData) error {
	f := excelize.NewFile()
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()
	sheetName := "Sheet1"
	err := f.SetCellValue(sheetName, "A1", "Group Path")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "B1", "Project ID")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "C1", "Project name")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "D1", "Last Commit")
	if err != nil {
		return err
	}
	err = f.SetCellValue(sheetName, "E1", "Last Commit Date")
	if err != nil {
		return err
	}
	for id, project := range projects {
		err = f.SetCellValue(sheetName, fmt.Sprintf("A%d", id+2), project.Group.FullPath)
		if err != nil {
			return err
		}
		err = f.SetCellValue(sheetName, fmt.Sprintf("B%d", id+2), project.Project.ID)
		if err != nil {
			return err
		}
		err = f.SetCellValue(sheetName, fmt.Sprintf("C%d", id+2), project.Project.Name)
		if err != nil {
			return err
		}
		err = f.SetCellValue(sheetName, fmt.Sprintf("D%d", id+2), project.Commit.Author.String())
		if err != nil {
			return err
		}
		err = f.SetCellValue(sheetName, fmt.Sprintf("E%d", id+2), project.Commit.Author.When.String())
		if err != nil {
			return err
		}
	}
	if err = f.SaveAs(fmt.Sprintf("_data/%s.xlsx", group.Path)); err != nil {
		return err
	}
	return nil
}
