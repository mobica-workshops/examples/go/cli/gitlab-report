package app

import (
	"fmt"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/app/types"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/app/utils"
	"gitlab.com/mobica-workshops/examples/go/cli/gitlab-report/config"
)

func GetGroups(git *gitlab.Client, config config.GitlabConfig) error {
	topLevel := true

	opt := &gitlab.ListGroupsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
		TopLevelOnly: &topLevel,
	}

	var myGroups []types.GroupRowData
	for {
		log.Info().
			Msg(fmt.Sprintf("Checking page: %d", opt.Page))
		groups, resp, err := git.Groups.ListGroups(opt)
		if err != nil {
			return err
		}
		for _, group := range groups {
			members, err := getGroupMembers(git, group)
			if err != nil {
				return err
			}

			myGroups = append(myGroups, types.GroupRowData{
				Group: group, Users: members,
			})
		}
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}

	for key, myGroup := range myGroups {
		commit, err := GetGroupById(git, myGroup.Group.ID, config)
		if err != nil {
			return err
		}
		myGroups[key].Commit = commit
	}

	utils.PrintGroups(myGroups)
	err := utils.GenerateGroupsReport(myGroups)
	if err != nil {
		return err
	}

	return nil
}

func GetGroupById(git *gitlab.Client, groupId int, config config.GitlabConfig) (*object.Commit, error) {
	var commit *object.Commit
	group, _, err := git.Groups.GetGroup(groupId, &gitlab.GetGroupOptions{})
	if err != nil {
		return commit, err
	}

	myProjects, err := getGroupObjects(git, group, 1, config)
	if err != nil {
		return commit, err
	}

	utils.PrintProjects(myProjects)
	err = utils.GenerateProjectsReport(group, myProjects)
	if err != nil {
		return commit, err
	}
	i := 0
	for _, myProject := range myProjects {
		if i == 0 {
			commit = myProject.Commit
		} else {
			if commit == nil || myProject.Commit.Author.When.After(commit.Author.When) {
				commit = myProject.Commit
			}
		}
		i++
	}

	return commit, nil
}

func getGroupObjects(git *gitlab.Client, group *gitlab.Group, level int, config config.GitlabConfig) ([]types.ProjectRowData, error) {
	log.Info().
		Msg(fmt.Sprintf("Checking projects for group: %s", group.FullPath))
	myProjects, err := getProjects(git, group, config)
	if err != nil {
		return myProjects, err
	}
	log.Info().
		Msg(fmt.Sprintf("Projects for group: %s checked", group.FullPath))
	log.Info().
		Msg(fmt.Sprintf("Searching subgroups for group: %s", group.FullPath))
	subGroupProjects, err := getSubGroups(git, group, level, config)
	log.Info().
		Msg(fmt.Sprintf("Subgroups for group: %s checked", group.FullPath))

	myProjects = append(myProjects, subGroupProjects...)
	if err != nil {
		return myProjects, err
	}
	return myProjects, nil
}

func getGroupMembers(git *gitlab.Client, group *gitlab.Group) ([]*gitlab.GroupMember, error) {
	var myMembers []*gitlab.GroupMember
	opt := &gitlab.ListGroupMembersOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
	}
	for {
		members, resp, err := git.Groups.ListGroupMembers(group.ID, opt)
		if err != nil {
			return nil, err
		}
		myMembers = append(myMembers, members...)
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}

	return myMembers, nil
}

func getSubGroups(git *gitlab.Client, group *gitlab.Group, level int, config config.GitlabConfig) ([]types.ProjectRowData, error) {
	myProjects := make([]types.ProjectRowData, 0)

	nextLevel := level + 1
	opt := &gitlab.ListSubGroupsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
	}
	for {
		groups, subResp, err := git.Groups.ListSubGroups(group.ID, opt)
		if err != nil {
			return myProjects, err
		}
		for _, subGroup := range groups {
			groupProjects, err := getGroupObjects(git, subGroup, nextLevel, config)
			if err != nil {
				return myProjects, err
			}
			myProjects = append(myProjects, groupProjects...)
		}
		if subResp.NextPage == 0 {
			break
		}
		opt.Page = subResp.NextPage
	}
	return myProjects, nil
}

func getProjects(git *gitlab.Client, group *gitlab.Group, config config.GitlabConfig) ([]types.ProjectRowData, error) {
	opt := &gitlab.ListGroupProjectsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
	}
	var myProjects []types.ProjectRowData
	for {
		log.Info().
			Msg(fmt.Sprintf("Searchin projects for group: %s ;page: %d", group.FullPath, opt.Page))
		projects, resp, err := git.Groups.ListGroupProjects(group.ID, opt)
		if err != nil {
			return myProjects, err
		}
		log.Info().
			Msg(fmt.Sprintf("Analizing projects for group: %s ;page: %d", group.FullPath, opt.Page))
		for _, project := range projects {
			log.Info().
				Msg(fmt.Sprintf("Starting checking project repository: %s for group: %s", project.Name, group.FullPath))
			commit, err := utils.GetProjectsRepos(project, config)
			if err != nil {
				log.Warn().
					Err(err).
					Msg(fmt.Sprintf("Error taking project repository: %s for group: %s", project.Name, group.FullPath))
				continue
			}
			log.Info().
				Msg(fmt.Sprintf("Starting checking project members: %s for group: %s", project.Name, group.FullPath))
			myUsers, err := getProjectMembers(git, project.ID)
			if err != nil {
				return myProjects, err
			}
			myProjects = append(myProjects, types.ProjectRowData{Group: group, Project: project, Users: myUsers, Commit: commit})
		}
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}
	return myProjects, nil
}

func getProjectMembers(git *gitlab.Client, projectId int) ([]*gitlab.ProjectUser, error) {
	opt := &gitlab.ListProjectUserOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
	}
	var myUsers []*gitlab.ProjectUser
	for {
		users, resp, err := git.Projects.ListProjectsUsers(projectId, opt)
		if err != nil {
			return nil, err
		}
		myUsers = append(myUsers, users...)
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}
	return myUsers, nil
}
