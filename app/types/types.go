package types

import (
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/xanzy/go-gitlab"
)

type ProjectRowData struct {
	Group   *gitlab.Group
	Project *gitlab.Project
	Users   []*gitlab.ProjectUser
	Commit  *object.Commit
}

type GroupRowData struct {
	Group  *gitlab.Group
	Users  []*gitlab.GroupMember
	Commit *object.Commit
}
